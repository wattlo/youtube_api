# YoutubeAPI

## Backend Requirements

* [Docker](https://www.docker.com/).
* [Docker Compose](https://docs.docker.com/compose/install/).

## Frontend Requirements

* Node.js (with `npm`).

## Backend local development

* Start the stack with Docker Compose:

```bash
docker-compose up --build
```

* Now you can open your browser and interact with these URLs:

Frontend, built with Docker, with routes handled based on the path: http://localhost

**Note**: The first time you start your stack, it might take a minute for it to be ready. 

If your Docker is not running in `localhost` (the URLs above wouldn't work) 

## Installation without Docker

* Backend
```
cd youtube_api
python3 -m venv env
source env/bin/activate

pip install -r backend/requirements.txt
python manage.py migrate
python manage.py runserver
```

* Frontend
```
cd youtube_api/frontend
npm install
npm run serve
```

* Open http://localhost
* Make sure other app is not listening to tcp:80 already
* Login form: just press `Login` button
# gunicorn~=19.9.0
requests~=2.25.0

Django~=3.1.3
djangorestframework~=3.12.2
django-environ~=0.4.5

django-cors-headers~=3.5.0

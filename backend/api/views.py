# pylint: disable=no-member
from django.contrib.auth.models import User

from rest_framework.decorators import api_view
from rest_framework import generics, permissions, viewsets, status
from rest_framework.response import Response
from rest_framework.reverse import reverse

from backend.api.serializers import (
    UserSerializer,
    ChannelSerializer
)
from backend.api.models import Channel
from backend.api.utils import get_channel_data


@api_view(['GET'])
def api_root(request):
    """API root"""
    return Response({
        'users': reverse('user-list', request=request),
        'channels': reverse('channel-list', request=request)
    })


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated]


class ChannelList(generics.ListCreateAPIView):
    queryset = Channel.objects.all()
    serializer_class = ChannelSerializer

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class ChannelSearch(generics.RetrieveAPIView):
    # queryset = Channel.objects.all()
    serializer_class = ChannelSerializer

    def retrieve(self, request, *args, **kwargs):
        channel_name = kwargs.get('query')
        if not channel_name:
            return Response(
                data='Missing channel name',
                status=status.HTTP_402_PAYMENT_REQUIRED
            )
        channel_data = get_channel_data(channel_name)
        if channel_data is None:
            return Response(data={})

        serializer = self.get_serializer(channel_data)
        return Response(serializer.data)


class ChannelDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Channel.objects.all()
    serializer_class = ChannelSerializer


class UserList(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserDetail(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

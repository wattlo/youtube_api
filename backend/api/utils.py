import logging
from typing import List
from django.conf import settings

from backend.youtube_api import YoutubeDataApi
from backend.api.models import Channel, Video

logger = logging.getLogger(__name__)


def get_channel_data(channel_name: str) -> Channel:
    """Get Yt DAPI channel data"""
    yt = YoutubeDataApi(settings.YOUTUBE_DATA_API_KEY)
    channel_id = yt.get_channel_id_from_user(
        username=channel_name
    )

    try:
        metadata = yt.get_channel_metadata(
            channel_id=channel_id
        )
    except Exception as exc:
        logging.error(str(exc))
        metadata = None

    if metadata is None:
        return None

    # Total number of views for all videos of the channel
    view_count = int(metadata.get('view_count', 0))
    video_count = int(metadata.get('video_count', 1))
    subscription_count = int(metadata.get('subscription_count', 0))

    # Avg Views number
    avg_view_count = int(view_count / video_count)

    channel = Channel(
        channel_id=metadata.get('channel_id'),
        title=metadata.get('title'),
        description=metadata.get('description'),
        keywords=metadata.get('keywords'),
        view_count=view_count,
        video_count=video_count,
        subscription_count=subscription_count,
        avg_view_count=avg_view_count,
        playlist_id_uploads=metadata.get('playlist_id_uploads'),
        topic_ids=metadata.get('topic_ids')
    )

    return channel


def search_channel(channel_name: str) -> List[Video]:
    """Check Yt DAPI channels by name"""
    yt = YoutubeDataApi(settings.YOUTUBE_DATA_API_KEY)

    # - Channel Name
    # - Total number of views for all videos of the channel
    # - Avg Views number
    list_dicts = yt.search(
        q=channel_name,
        max_results=5,
        order_by="relevance",
        search_type="channel",
    )
    results = []
    for d in list_dicts:
        results.append(
            Video(
                channel_id=d.get('channel_id'),
                channel_title=d.get('channel_title'),
                video_publish_date=d.get('video_publish_date'),
                video_title=d.get('video_title'),
                video_description=d.get('video_description'),
                video_category=d.get('video_category'),
                video_thumbnail=d.get('video_thumbnail'),
            )
        )

    return results

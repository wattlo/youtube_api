# pylint: disable=too-few-public-methods
from django.db import models


class Channel(models.Model):
    """Youtube channel"""
    created = models.DateTimeField(auto_now_add=True)
    channel_id = models.CharField(max_length=100, unique=True)
    title = models.CharField(max_length=1024, blank=True, default='')
    account_creation_date = models.DateTimeField(null=True)
    keywords = models.TextField(blank=True)
    description = models.TextField(blank=True)
    view_count = models.BigIntegerField(default=0)
    video_count = models.BigIntegerField(default=0)
    subscription_count = models.BigIntegerField(default=0)
    avg_view_count = models.BigIntegerField(default=0)
    playlist_id_likes = models.CharField(max_length=1024, blank=True, default='')
    playlist_id_uploads = models.CharField(max_length=1024, blank=True, default='')
    topic_ids = models.CharField(max_length=1024, blank=True, default='')
    country = models.CharField(max_length=128, blank=True, default='')

    class Meta:
        ordering = ['created']


class Video(models.Model):
    """Youtube Video"""
    created = models.DateTimeField(auto_now_add=True)
    video_id = models.CharField(max_length=100, unique=True)
    # could be FK, but faster denormalize
    channel_id = models.CharField(max_length=100, db_index=True)
    channel_title = models.CharField(max_length=1024, blank=True, default='')

    video_publish_date = models.DateTimeField()
    video_title = models.CharField(max_length=1024, blank=True, default='')
    video_description = models.TextField(blank=True)
    video_category = models.CharField(max_length=100, blank=True, default='')
    video_view_count = models.BigIntegerField(default=0)
    video_comment_count = models.BigIntegerField(default=0)
    video_like_count = models.BigIntegerField(default=0)
    video_dislike_count = models.BigIntegerField(default=0)
    video_thumbnail = models.TextField(blank=True)
    video_tags = models.CharField(max_length=512, blank=True, default='')

    class Meta:
        ordering = ['created']

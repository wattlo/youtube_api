# pylint: disable=too-few-public-methods
from django.contrib.auth.models import User
from rest_framework import serializers
from .models import Channel, Video


class UserSerializer(serializers.HyperlinkedModelSerializer):
    # channels = serializers.PrimaryKeyRelatedField(many=True, queryset=Channel.objects.all())

    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups']


class ChannelSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')

    class Meta:
        model = Channel
        fields = '__all__'


class VideoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Video
        fields = '__all__'

from django.urls import include, path
from backend.api import views

urlpatterns = [
    path('', views.api_root),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    path('api/v1/channels/', views.ChannelList.as_view(), name='channel-list'),
    path('api/v1/channels/<int:pk>/', views.ChannelDetail.as_view(), name='channel-detail'),
    path('api/v1/channels/<str:query>/', views.ChannelSearch.as_view(), name='channel-search'),

    path('api/v1/users/', views.UserList.as_view(), name='user-list'),
    path('api/v1/users/<int:pk>/', views.UserDetail.as_view(), name='user-detail'),
]
